#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <wait.h>
#include <stdlib.h>
#include <stdio.h>


int main ()
{
    pid_t pid;
    pid = fork ();
    if (pid > 0) {
        printf("Это Родительский процесс pid=%d\n", getpid());
        pid = fork ();
    }

    char buffer[12];
    int millisec;
    struct tm* tm_info;
    struct timeval tv;

    gettimeofday(&tv, NULL);

    millisec = tv.tv_usec   ;

    tm_info = localtime(&tv.tv_sec);
    strftime(buffer, 12, "%H:%M:%S", tm_info);
    printf("%s.%03d\n", buffer, millisec);

    if (pid == 0) {
        printf ("Это Дочерний процесс, его pid=%d\n", getpid());
        printf ("А pid его Родительского процесса=%d\n", getppid());
    }

    if (pid > 0){
        system("ps -x | grep os");
        for (int i = 0; i < 2; ++i) {
            wait(NULL);
        }
    }
}