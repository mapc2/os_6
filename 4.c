#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <wait.h>
#include <fcntl.h>

typedef struct {
    pid_t pid;
    char *filename;
} proc_info_struct;

char *string;

int find_string_in_file(char *filename, char *search_string) {
    int fd = open(filename, O_RDONLY);
    if (fd == -1) {
        return -1;
    }

    int i = 0;
    int total_bytes = 0;
    size_t len_string = strlen(search_string);
    char c = 0;

    ssize_t bytes_read;
    bytes_read = read(fd, &c, sizeof(char));
    while (bytes_read > 0) {
        total_bytes++;
        if (search_string[i] == c)
            i++;
        else
            i = 0;

        if (len_string == i) {
            close(fd);
            return total_bytes;
        }
        bytes_read = read(fd, &c, sizeof(char));
    }

    close(fd);
    return 0;
}

void *process_file(void *args) {
    proc_info_struct *proc_info = (proc_info_struct *) args;
    int total_bytes = find_string_in_file(proc_info->filename, string);
    if (total_bytes > 0) {
        printf("%d %s %d\n", proc_info->pid, proc_info->filename, total_bytes);
    }

    free(proc_info->filename);
    free(proc_info);
    return NULL;
}

void file_path(char *dest, char * path, char *const name) {
    strcpy(dest, path);
    strcat(dest, "/");
    strcat(dest, name);
}

void find_string_in_folder(char *path, int processes_count) {
    pid_t pid;
    int num_of_proc = 0;
    DIR *dir_stream;
    struct dirent *dir_entry;
    dir_stream = opendir(path);

    if (!dir_stream) {
        return;
    }

    proc_info_struct *proc_info;

    dir_entry = readdir(dir_stream);
    while (dir_entry != NULL) {
        char *entry_name = dir_entry->d_name;

        if (!strcmp(".", entry_name) || !strcmp("..", entry_name)) {
            dir_entry = readdir(dir_stream);
            continue;
        }

        char *full_path = malloc(strlen(entry_name) + strlen(path) + 2);
        file_path(full_path, path, entry_name);

        struct stat entry_info;
        lstat(full_path, &entry_info);

        if (S_ISDIR(entry_info.st_mode)) {
            dir_entry = readdir(dir_stream);
            continue;
        }
        else {
            if (S_ISREG(entry_info.st_mode)) {
                while (num_of_proc > processes_count)
                {
                    wait(NULL);
                    num_of_proc--;
                }
                pid = fork();
                num_of_proc ++;
                if (pid == 0){
                    printf("%d - начал работу\n", getpid());
                    proc_info = malloc(sizeof(proc_info_struct));
                    proc_info->pid = getpid();
                    proc_info->filename = full_path;

                    process_file(proc_info);
                    printf("%d - закончил работу\n", getpid());
                    exit(0);
                }
            }
        }
        dir_entry = readdir(dir_stream);
    }

    while (num_of_proc > 0) {
        wait(NULL);
        num_of_proc --;
    }

    closedir(dir_stream);
}


int main(int argc, char *argv[]) {
//    argv[1] = "/home/mapc/CLionProjects/os_6/test";
//    argv[2] = "456";
//    argv[3] = "2";
    int processes_count = (int)*argv[3];
    if (processes_count < 1) {
        return 1;
    }
    string = argv[2];

    find_string_in_folder(argv[1], processes_count);
}